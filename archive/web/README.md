####기존에 공부하면서 만들었던 HTML 페이지
굉장히 Old한 스타일이기 때문에 사용할 수는 없었다.


### ex
HTML을 공부하면서 내가 배운 TAG들을 정리한 디렉터리

### image
HTML에 삽입된 그림들
Pixabay, Unsplash 에서 가져온 저작권 없는 사진들

### JS
HTML에 적용된 JavaScript 소스파일이다.


### service
main.index에서 각각의 서비스들로 이용할 때 활용하려 했으나
굉장히 Old한 스타일의 HTML이기에 작성하지 않았다.

###style
HTML에 적용돼 있는 CSS 소스파일이다.

main.html : 메인 페이지
loginmain.html : 로그인 페이지
afterlogin.html : 로그인 이후 페이지
question.html : 문의사항 페이지
