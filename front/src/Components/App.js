import React from "react";
import { ThemeProvider } from "styled-components";
import GlobalStyles from "../Styles/GlobalStyles";
import Helmet from "./Helmet";
import Theme from "../Styles/Theme";
import { gql } from "@apollo/client";
import { useQuery } from "@apollo/react-hooks";
import { BrowserRouter } from "react-router-dom";
import Router from "../Routes/Router";

const QUERY = gql`
  {
    isLoggedIn @client
  }
`;

export default () => {
  const {
    data: { isLoggedIn },
  } = useQuery(QUERY);

  return (
    <ThemeProvider theme={Theme}>
      <>
        <GlobalStyles />
        <Helmet />
        <BrowserRouter>
          <Router isLoggedIn={isLoggedIn} />
        </BrowserRouter>
      </>
    </ThemeProvider>
  );
};
