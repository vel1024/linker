import React from "react";
import styled from "styled-components";
import Button from "./Button";
import Laptop from "../Imgs/laptop.jpg";
import Phone from "../Imgs/leftImage.jpg";

const Main = styled.main`
  display: flex;
  flex-direction: column;
`;

const Section = styled.section`
  padding-top: 8rem;
  padding-bottom: 5rem;
  max-width: none;
  &.color-section {
    background-color: #341f97;
  }
`;

const SectionBox = styled.div`
  max-width: 80%;
  display: grid;
  grid-column-gap: 5%;
  grid-template-columns: 40% 50%;
  align-items: center;
  margin: 0 auto;
`;

const DescriptionBox = styled.div`
  p {
    font-size: 1.1rem;
    word-spacing: 7px;
    line-height: 1.4;
  }
`;

const TitleBox = styled.div`
  margin-bottom: 10px;
  font-size: 2rem;
  font-weight: 600;

  &.color-title {
    color: #ffffff;
  }
`;

const IllustrationBox = styled.div``;

const Image = styled.img`
  width: 100%;
  height: 100%;
`;

const ColorBox = styled.div`
  max-width: 80%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ButtonBox = styled.div`
  display: flex;
  flex-direction: row;
  padding: 10px;
  button {
    cursor: pointer;
    width: 200px;
    height: 50px;
    background-color: #474787;
    color: white;
    border-radius: 10px;
    &:not(:last-child) {
      margin-right: 10px;
    }
    &:last-child {
      background-color: white;
      border: 1px solid #474787;
      color: #474787;
    }
  }
`;

export default () => {
  return (
    <Main>
      <Section>
        <SectionBox>
          <DescriptionBox>
            <TitleBox>Lorem Ipsum</TitleBox>
            <p>
              What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the
              printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s, when an
              unknown printer took a galley of type and scrambled it to make a
              type specimen book. It has survived not only five centuries, but
              also the leap into electronic typesetting, remaining essentially
              unchanged. It was popularised in the 1960s with the release of
              Letraset sheets containing Lorem Ipsum passages, and more recently
              with desktop publishing software like Aldus PageMaker including
              versions of Lorem Ipsum
            </p>
          </DescriptionBox>
          <IllustrationBox>
            <Image src={Laptop} alt={"Back Image"} />
          </IllustrationBox>
        </SectionBox>
      </Section>
      <Section>
        <SectionBox>
          <IllustrationBox>
            <Image src={Phone} alt={"Back Image"}></Image>
          </IllustrationBox>
          <DescriptionBox>
            <TitleBox>Why do we use it?</TitleBox>
            <p>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using 'Content here,
              content here', making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for 'lorem ipsum' will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </p>
          </DescriptionBox>
        </SectionBox>
      </Section>
      <Section className="color-section">
        <ColorBox>
          <TitleBox className="color-title">Ipsum Lorem</TitleBox>
          <ButtonBox>
            <Button text={"Ipsum"} />
            <Button text={"Lorem"} />
          </ButtonBox>
        </ColorBox>
      </Section>
    </Main>
  );
};
