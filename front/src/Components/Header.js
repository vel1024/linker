import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComments } from "@fortawesome/free-solid-svg-icons";

const Header = styled.header`
  display: block;
`;

const Nav = styled.nav`
  height: 70px;
  width: 100%;
  position: fixed;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
  background-color: rgba(255, 255, 255);
  z-index: 100;
`;

const HeaderBox = styled.div`
  height: 100%;
  max-width: 80%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 0 auto;
`;

const HeaderTitleBox = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  svg {
    font-size: 1.5rem;
    margin-right: 10px;
  }
`;

const HeaderTitle = styled.span`
  font-size: 1.5rem;
  font-weight: 600;
`;

const HeaderMenuNav = styled.nav`
  display: flex;
  justify-content: flex-start;
  width: 100%;
  height: 80%;
`;

const HeaderMenuList = styled.ul`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex: 1;
`;

const HeaderMenuItem = styled.li`
  font-size: 1.2rem;
  width: auto;
  padding: 1rem;
`;

const StartBox = styled.div`
  display: flex;
  align-items: center;
`;

const StartLink = styled(Link)`
  background-color: white;
  color: black;
  border: 2px solid #3dc1d3;
  padding: 16px 32px;
  margin: 4px 2px;
  transition-duration: 0.4s;
  border-radius: 8px;
  &:hover {
    background-color: #3dc1d3;
    color: white;
  }
`;

export default ({ btnStateProp }) => {
  const [btnState] = useState(btnStateProp ? btnStateProp : "main");

  return (
    <Header>
      <Nav>
        <HeaderBox>
          <HeaderTitleBox>
            <FontAwesomeIcon icon={faComments} />
            <HeaderTitle>
              <Link to="/">Linker</Link>
            </HeaderTitle>
          </HeaderTitleBox>
          <HeaderMenuNav>
            <HeaderMenuList>
              <HeaderMenuItem>
                <Link to="/forum">Forum</Link>
              </HeaderMenuItem>
              <HeaderMenuItem>
                <Link to="/features">Features</Link>
              </HeaderMenuItem>
              <HeaderMenuItem>
                <Link to="/news">News</Link>
              </HeaderMenuItem>
            </HeaderMenuList>
            {btnState === "main" ? (
              <>
                <StartBox>
                  <StartLink to="/auth">Login</StartLink>
                </StartBox>
              </>
            ) : (
              <>
                <StartBox>
                  <StartLink to="/chat">Start</StartLink>
                </StartBox>
              </>
            )}
          </HeaderMenuNav>
        </HeaderBox>
      </Nav>
    </Header>
  );
};
