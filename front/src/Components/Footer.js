import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Footer = styled.footer`
  width: 100%;
  background-color: #ecf0f1;
  border-top: 1px solid #ebeaeb;
`;

const MenuContainer = styled.div`
  padding: 5rem 0;
`;

const MenuNav = styled.nav`
  display: grid;
  grid-gap: 2rem;
  max-width: 80%;
  margin: 0 auto;
  grid-template-columns: repeat(3, 1fr);
`;

const MenuBlock = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const MenuTitle = styled.p`
  padding: 10px;
  font-weight: 600;
  font-size: 1.3rem;
`;

const MenuList = styled.ul`
  line-height: 2;
`;

const MenuItem = styled.li`
  text-align: center;
`;

const CopyRightBox = styled.div`
  width: 100%;
`;

const CopyRightBlock = styled.div`
  max-width: 80%;
  margin: 0 auto;
  height: 2.5rem;
  display: flex;
  align-items: center;
`;

const CopyRightSpan = styled.span`
  font-size: 1rem;
`;

export default () => {
  return (
    <Footer>
      <MenuContainer>
        <MenuNav>
          <MenuBlock>
            <MenuTitle>CONCEPT</MenuTitle>
            <MenuList>
              <MenuItem>
                <Link to="/features">Features</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/goals">Goals</Link>
              </MenuItem>
            </MenuList>
          </MenuBlock>
          <MenuBlock>
            <MenuTitle>CONTACT</MenuTitle>
            <MenuList>
              <MenuItem>
                <Link to="/forum">Forum</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/email">Email</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/blog">Blog</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/github">Github</Link>
              </MenuItem>
            </MenuList>
          </MenuBlock>
          <MenuBlock>
            <MenuTitle>COMPANY</MenuTitle>
            <MenuList>
              <MenuItem>
                <Link to="/about">About US</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/news">News</Link>
              </MenuItem>
            </MenuList>
          </MenuBlock>
        </MenuNav>
      </MenuContainer>
      <CopyRightBox>
        <CopyRightBlock>
          <CopyRightSpan>
            @ Copyright 2020 Linker. All rights reserved. Various trademarks
            held by their respective owners.
          </CopyRightSpan>
        </CopyRightBlock>
      </CopyRightBox>
    </Footer>
  );
};
