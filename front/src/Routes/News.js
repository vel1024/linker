import React from "react";
import styled from "styled-components";
import Header from "../Components/Header";
import Footer from "../Components/Footer";

const Main = styled.main`
  display: flex;
  flex-direction: column;
`;

const Section = styled.section`
  padding-top: 8rem;
  padding-bottom: 5rem;
  max-width: none;
  &.title-section {
    background-color: black;
  }
`;

const SectionBox = styled.div`
  max-width: 80%;
  margin: 0 auto;
  &.title-section-box {
    max-width: 100%;
  }
`;

const DescriptionBox = styled.div`
  display: flex;
  flex-direction: column;
`;

const TitleBox = styled.div`
  text-align: center;
  font-size: 2rem;
  font-weight: 600;
  margin-bottom: 20px;
  &.top-title-box {
    color: white;
  }
`;

export default () => {
  return (
    <>
      <Header />
      <Main>
        <Section className="title-section">
          <SectionBox className="title-section-box">
            <DescriptionBox>
              <TitleBox className="top-title-box">What's New?</TitleBox>
            </DescriptionBox>
          </SectionBox>
        </Section>
      </Main>
      <Footer />
    </>
  );
};
