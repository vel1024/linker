import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import Input from "../../Components/Input";
import Button from "../../Components/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faTwitter,
  faGithub,
  faGoogle,
} from "@fortawesome/free-brands-svg-icons";
import loginBackground from "../../Imgs/login-background4.jpg";

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-image: url(${loginBackground});
  background-size: cover;
`;

const SubWrapper = styled.div`
  background-color: rgba(57, 65, 74, 0.5);
  border-radius: 7px;
  padding: 5%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
`;

const Box = styled.div`
  display: flex;
`;

const TitleContainer = styled(Box)`
  margin-bottom: 20px;
`;

const Title = styled.span`
  font-size: 30px;
  font-family: "Raleway", sans-serif;
`;

const StateChanger = styled(Box)`
  margin-bottom: 15px;
`;

const ToggleSpan = styled.span`
  margin-left: 5px;
  cursor: pointer;
  color: #079992;
`;

const Form = styled(Box)`
  form {
    display: flex;
    flex-direction: column;
    padding: 5px 5px;
    margin-bottom: 15px;
    input {
      font-size: 15px;
      margin-bottom: 10px;
    }
    button {
      background-color: #1b9cfc;
      color: white;
      border-radius: 10px;
      padding: 10px 5px;
      font-size: 15px;
    }
  }
`;

const SocialLogin = styled(Box)`
  display: flex;
  a {
    &:not(:last-child) {
      margin-right: 20px;
    }
  }
  font-size: 30px;
  opacity: 0.8;
`;

const StyledLink = styled(Link)`
  color: white;
`;

export default ({
  action,
  setAction,
  email,
  password,
  password2,
  username,
  phoneNum,
  onSubmit,
}) => (
  <>
    <Wrapper>
      <SubWrapper>
        <TitleContainer>
          <StyledLink to="/">
            <Title>Linker</Title>
          </StyledLink>
        </TitleContainer>
        <Form>
          {action === "logIn" ? (
            <>
              <Helmet>
                <title>Log In</title>
              </Helmet>
              <form onSubmit={onSubmit}>
                <Input placeholder={"Email"} type="email" {...email} />
                <Input placeholder={"Password"} type="password" {...password} />
                <Button text={"Log In"} />
              </form>
            </>
          ) : (
            <>
              <Helmet>
                <title>Sign Up</title>
              </Helmet>
              <form onSubmit={onSubmit}>
                <Input placeholder={"Email"} type="email" {...email} />
                <Input placeholder={"UserName"} {...username} />
                <Input placeholder={"Password"} type="password" {...password} />
                <Input placeholder={"PhoneNumber"} {...phoneNum} />
                <Button text={"Sign Up"} />
              </form>
            </>
          )}
        </Form>
        <StateChanger>
          {action === "logIn" ? (
            <>
              Don't you have an account ?
              <ToggleSpan onClick={() => setAction("signUp")}>
                {" "}
                Sign Up{" "}
              </ToggleSpan>
            </>
          ) : (
            <>
              Did you have an account ?
              <ToggleSpan onClick={() => setAction("logIn")}>
                {" "}
                Log in{" "}
              </ToggleSpan>
            </>
          )}
        </StateChanger>
        <SocialLogin>
          <StyledLink to="/facebook">
            <FontAwesomeIcon icon={faFacebook} />
          </StyledLink>
          <StyledLink to="/google">
            <FontAwesomeIcon icon={faGoogle} />
          </StyledLink>
          <StyledLink to="/twitter">
            <FontAwesomeIcon icon={faTwitter} />
          </StyledLink>
          <StyledLink to="/github">
            <FontAwesomeIcon icon={faGithub} />
          </StyledLink>
        </SocialLogin>
      </SubWrapper>
    </Wrapper>
  </>
);
