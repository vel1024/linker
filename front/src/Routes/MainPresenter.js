import React from "react";
import Header from "../Components/Header";
import Main from "../Components/Main";
import Footer from "../Components/Footer";
import { gql } from "@apollo/client";
import { useQuery } from "@apollo/react-hooks";

const checkLogin = gql`
  {
    isLoggedIn @client
  }
`;

export default () => {
  const {
    data: { isLoggedIn },
  } = useQuery(checkLogin);

  let btnState;

  if (isLoggedIn) {
    btnState = "roomlist";
  } else {
    btnState = "main";
  }

  return (
    <>
      <Header btnStateProp={btnState} />
      <Main />
      <Footer />
    </>
  );
};
