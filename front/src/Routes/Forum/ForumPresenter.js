import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Header from "../../Components/Header";
import Footer from "../../Components/Footer";
import Button from "../../Components/Button";

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 4.3rem;
  padding-bottom: 5rem;
`;

const ContentHeader = styled.header`
  background-color: black;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 300px;
`;

const HeaderSpan = styled.span`
  color: white;
  font-size: 3rem;
`;

const ContentMain = styled.main`
  width: 80%;
  margin: 0 auto;
`;

const MainTabBox = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 20px 0px;
  width: 100%;
  span {
    font-size: 1.5rem;
    color: black;
  }
  button {
    width: 70px;
    height: 30px;
    border-radius: 4px;
    border: 1px solid black;
    background-color: white;
    cursor: pointer;
  }
`;

const MainSection = styled.section``;

const PostList = styled.ul`
  display: flex;
  flex-direction: column;
  margin: 20px 0px;
`;

const PostItem = styled.li`
  display: flex;
  flex-direction: row;
  &.post-header {
    background-color: #eaeaea;
  }
  &.post-footer {
    background-color: #eaeaea;
    height: 30px;
  }
`;

const TopicList = styled.ul`
  width: 100%;
  display: flex;
  flex-direction: row;
  margin: 10px 0px;
`;

const TopicItem = styled.li`
  font-size: 1.5rem;
  padding: 10px;
  &.topic-title {
    text-align: left;
    width: 55%;
  }
  &.topic-view,
  &.topic-reply,
  &.topic-date {
    text-align: center;
    width: 15%;
  }
`;

const PaginationBox = styled.div`
  width: 100%;
  margin-bottom: 20px;
`;

const PaginationList = styled.ul`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const PaginationItem = styled.li`
  font-size: 1.5rem;
  a {
    margin: 0px 10px;
  }
`;

const PaginationSpan = styled.span`
  background: #eaeaea;
  border-radius: 5px;
  margin: 0px 10px;
`;

export default () => {
  return (
    <>
      <Header />
      <ContentWrapper>
        <ContentHeader>
          <HeaderSpan>Forums</HeaderSpan>
        </ContentHeader>
        <ContentMain>
          <MainTabBox>
            <HeaderSpan>Viewing 20 topics</HeaderSpan>
            <Button text="Post" />
          </MainTabBox>
          <MainSection>
            <PostList>
              <PostItem className="post-header">
                <TopicList>
                  <TopicItem className="topic-title">Topic</TopicItem>
                  <TopicItem className="topic-view">View</TopicItem>
                  <TopicItem className="topic-reply">Reply</TopicItem>
                  <TopicItem className="topic-date">Date</TopicItem>
                </TopicList>
              </PostItem>
              <PostItem className="post-content">
                <TopicList>
                  <TopicItem className="topic-title">Title-One</TopicItem>
                  <TopicItem className="topic-view">One-View</TopicItem>
                  <TopicItem className="topic-reply">Zero-Reply</TopicItem>
                  <TopicItem className="topic-date">2020-Date</TopicItem>
                </TopicList>
              </PostItem>
              <PostItem className="post-footer" />
            </PostList>
          </MainSection>
          <PaginationBox>
            <PaginationList>
              <PaginationItem>
                <PaginationSpan>1</PaginationSpan>
                <Link to="/">2</Link>
                <Link to="/">3</Link>
              </PaginationItem>
            </PaginationList>
          </PaginationBox>
        </ContentMain>
      </ContentWrapper>
      <Footer />
    </>
  );
};
