import React from "react";
import styled from "styled-components";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import ChatImage from "../Imgs/test.JPG";

const Main = styled.main`
  display: flex;
  flex-direction: column;
`;

const Section = styled.section`
  padding-top: 8rem;
  padding-bottom: 5rem;
  max-width: none;
`;

const SectionBox = styled.div`
  max-width: 80%;
  margin: 0 auto;
`;

const ImageSection = styled.div`
  width: 100%;
  overflow: hidden;
  position: relative;
`;

const ItemContainer = styled.div`
  display: flex;
  transform: translateX(-75deg);
`;

const ItemBox = styled.div`
  padding: 0 10px;
`;

const ArrowContainer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
`;

const ArrowWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const ArrowLeftBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 40px;
  width: 64px;
  height: 64px;
  background-color: rgba(35, 39, 42, 0.3);
  border-radius: 50%;
  cursor: pointer;
`;

const ArrowRightBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 40px;
  width: 64px;
  height: 64px;
  background-color: rgba(35, 39, 42, 0.3);
  border-radius: 50%;
  cursor: pointer;
`;

const ArrowSvg = styled.svg`
  width: 32px;
  height: 32px;
  &.arrow-left-svg {
    transform: rotate(180deg);
  }
  &.arrow-right-svg {
    transform: rotate(0deg);
  }
`;

const LeftArrowPath = styled.path`
  fill-rule: evenodd;
  clip-rule: evenodd;
  fill: white;
`;

const RightArrowPath = styled.path`
  fill-rule: evenodd;
  clip-rule: evenodd;
  fill: white;
`;

const DescriptionBox = styled.div`
  display: flex;
  flex-direction: column;
`;

const TitleBox = styled.div`
  text-align: center;
  font-size: 2rem;
  font-weight: 600;
  margin-bottom: 20px;
  &.paragraph-title {
    color: #3dc1d3;
    text-align: left;
  }
  &.desc-header-title {
    margin-bottom: 50px;
  }
`;

const ParagraphContainer = styled.div`
  display: grid;
  grid-template-columns: 30% 30% 30%;
  grid-column-gap: 5%;
`;

const ParagraphBox = styled.div`
  display: flex;
  flex-direction: column;
  &:not(:last-child) {
    margin-right: 20px;
  }
  p {
    font-size: 1.2rem;
    line-height: 1.5;
  }
`;

const Image = styled.img`
  width: 100%;
  height: auto;
  border-radius: 10px;
`;

export default () => {
  return (
    <>
      <Header />
      <Main>
        <Section>
          <SectionBox>
            <DescriptionBox>
              <TitleBox className="desc-header-title">Why Linker ?</TitleBox>
              <ParagraphContainer>
                <ParagraphBox>
                  <TitleBox className="paragraph-title">Comfortable</TitleBox>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book.
                  </p>
                </ParagraphBox>
                <ParagraphBox>
                  <TitleBox className="paragraph-title">Easy</TitleBox>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book.
                  </p>
                </ParagraphBox>
                <ParagraphBox>
                  <TitleBox className="paragraph-title">Colorful</TitleBox>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book.
                  </p>
                </ParagraphBox>
              </ParagraphContainer>
            </DescriptionBox>
          </SectionBox>
        </Section>
        <Section>
          <TitleBox className="desc-header-title">Look Around Linker</TitleBox>
          <ImageSection className="image-section">
            <ItemContainer>
              <ItemBox>
                <Image src={ChatImage} alt={"Back Image"} />
              </ItemBox>
              <ItemBox>
                <Image src={ChatImage} alt={"Back Image"} />
              </ItemBox>
              <ItemBox>
                <Image src={ChatImage} alt={"Back Image"} />
              </ItemBox>
              <ItemBox>
                <Image src={ChatImage} alt={"Back Image"} />
              </ItemBox>
            </ItemContainer>
            <ArrowContainer>
              <ArrowWrapper>
                <ArrowLeftBox>
                  <ArrowSvg fill="none" className="arrow-left-svg">
                    <LeftArrowPath d="M22.2398 17.0778L11.8576 27.5689C11.2532 28.1437 10.3287 28.1437 9.75984 27.5689C9.19095 26.994 9.19095 26.0599 9.75984 25.4491L19.1109 16L9.75984 6.5509C9.19095 5.97605 9.19095 5.00599 9.75984 4.43114C10.3287 3.85629 11.2532 3.85629 11.8576 4.43114L22.2398 14.9581C22.8087 15.5329 22.8087 16.4671 22.2398 17.0778Z" />
                  </ArrowSvg>
                </ArrowLeftBox>
                <ArrowRightBox>
                  <ArrowSvg fill="none" className="arrow-right-svg">
                    <RightArrowPath d="M22.2398 17.0778L11.8576 27.5689C11.2532 28.1437 10.3287 28.1437 9.75984 27.5689C9.19095 26.994 9.19095 26.0599 9.75984 25.4491L19.1109 16L9.75984 6.5509C9.19095 5.97605 9.19095 5.00599 9.75984 4.43114C10.3287 3.85629 11.2532 3.85629 11.8576 4.43114L22.2398 14.9581C22.8087 15.5329 22.8087 16.4671 22.2398 17.0778Z" />
                  </ArrowSvg>
                </ArrowRightBox>
              </ArrowWrapper>
            </ArrowContainer>
          </ImageSection>
        </Section>
      </Main>
      <Footer />
    </>
  );
};
