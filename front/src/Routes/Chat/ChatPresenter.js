import React from "react";
import styled from "styled-components";
import Input from "../../Components/Input";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";

const Wrapper = styled.div`
  display: block;
  width: 100%;
  height: 100vh;
  background-color: #2f3136;
`;

const ServerListBox = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 72px;
  background-color: #202225;
  color: white;
`;

const ServerNav = styled.nav`
  display: flex;
  flex-direction: column;
`;

const ServerBox = styled.div``;

const MainContainer = styled.div`
  position: absolute;
  top: 0;
  left: 72px;
  right: 0;
  bottom: 0;
`;

const MainContentBox = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex: 1 1 auto;
`;

const MainSideBar = styled.div`
  background-color: #2f3136;
  color: white;
`;

const MainBox = styled.div`
  width: 100%;
`;

const HeaderSection = styled.section`
  background-color: #36393f;
  color: white;
  padding: 15px;
`;

const SectionContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: auto;
`;

const ChatSection = styled.section`
  background-color: #36393f;
  color: white;
  flex: 1 1 auto;
`;

const MessageWrapper = styled.div`
  position: relative;
  display: flex;
  flex: 1 1 auto;
`;

const ScrollWrapper = styled.div`
  overflow-y: scroll;
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  &::-webkit-scrollbar {
    width: 16px;
    height: 16px;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #202225;
    border-color: #36393f;
  }
  &::-webkit-scrollbar-thumb,
  &::-webkit-scrollbar-track {
    border-width: 4px;
    border-radius: 8px;
  }
`;

const ScrollInner = styled.div``;

const ScrollInnerTitle = styled.div`
  padding: 10px;
  p {
    text-align: center;
  }
`;

const CardWrapper = styled.div`
  padding: 5px;
  text-align: center;
  &:last-child {
    margin-bottom: 10px;
  }
`;

const StyleForm = styled.form``;

const InputArea = styled.div`
  display: flex;
  flex-direction: row;
`;

const AttachArea = styled.div``;

const TextArea = styled.div``;

const ButtonArea = styled.div``;

const PeopleSection = styled.section`
  background-color: #2f3136;
  color: white;
  flex: 0 1 30%;
  min-width: 360px;
  max-width: 420px;
`;

export default () => {
  return (
    <Wrapper>
      <ServerListBox>
        <ServerNav>
          <ServerBox>Test</ServerBox>
        </ServerNav>
      </ServerListBox>
      <MainContainer>
        <MainContentBox>
          <MainSideBar>채널 및 DM 목록</MainSideBar>
          <MainBox>
            <HeaderSection>상태 표시바</HeaderSection>
            <SectionContainer>
              <ChatSection>
                <MessageWrapper>
                  <ScrollWrapper>
                    <ScrollInner>
                      <ScrollInnerTitle>
                        <p>.. Server 에 오신 것을 환영합니다.</p>
                        <p>We Introduce some guides below.</p>
                      </ScrollInnerTitle>
                      <CardWrapper>초대하기</CardWrapper>
                      <CardWrapper>데스크톱 앱 다운로드</CardWrapper>
                      <CardWrapper>아이콘</CardWrapper>
                    </ScrollInner>
                  </ScrollWrapper>
                </MessageWrapper>
                <StyleForm>
                  <InputArea>
                    <AttachArea>
                      <FontAwesomeIcon icon={faPlusSquare} />
                    </AttachArea>
                    <TextArea>
                      <Input placeholder="# 보낼 메시지 입력" />
                    </TextArea>
                    <ButtonArea></ButtonArea>
                  </InputArea>
                </StyleForm>
              </ChatSection>
              <PeopleSection>활동중인 친구 리스트</PeopleSection>
            </SectionContainer>
          </MainBox>
        </MainContentBox>
      </MainContainer>
    </Wrapper>
  );
};
