import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";
import Auth from "./Auth/AuthContainer";
import About from "./About";
import Main from "./MainPresenter";
import Forum from "./Forum/ForumPresenter";
import Features from "./Features";
import News from "./News";
import Chat from "./Chat/ChatContainer";

const LoggedInRoutes = () => (
  <Switch>
    <Route exact path="/" component={Main} />
    <Route path="/chat" component={Chat} />
  </Switch>
);

const LoggedOutRoutes = () => (
  <Switch>
    <Route exact path="/" component={Main} />
    <Route path="/about" component={About} />
    <Route path="/auth" component={Auth} />
    <Route path="/forum" component={Forum} />
    <Route path="/features" component={Features} />
    <Route path="/news" component={News} />
  </Switch>
);

const AppRouter = ({ isLoggedIn }) =>
  isLoggedIn ? <LoggedInRoutes /> : <LoggedOutRoutes />;

AppRouter.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
};

export default AppRouter;
