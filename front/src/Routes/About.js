import React from "react";
import styled from "styled-components";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import Laptop from "../Imgs/laptop2.jpg";

const AboutMain = styled.main``;

const Section = styled.section`
  padding-top: 8rem;
  padding-bottom: 5rem;
  background-image: url(${Laptop});
  background-size: cover;
  &.none-image-section {
    background: none;
  }
`;

const SectionBox = styled.div`
  max-width: 80%;
  margin: 0 auto;
`;

const TitleBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 2;
  &.none-image-box {
    display: grid;
    grid-template-columns: 40% 50%;
    grid-column-gap: 5%;
  }
`;

const Title = styled.span`
  color: blanchedalmond;
  font-size: 2rem;
  &.none-image-title {
    color: black;
    text-align: center;
  }
`;

const Paragraph = styled.p`
  color: blanchedalmond;
  font-size: 1.3rem;
  &.none-image-paragraph {
    color: black;
    font-size: 1rem;
  }
`;

export default () => {
  return (
    <>
      <Header />
      <AboutMain>
        <Section>
          <SectionBox>
            <TitleBox>
              <Title>WelCome</Title>
              <Paragraph>
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form, by
                injected humour, or randomised words which don't look even
                slightly believable. If you are going to use a passage of Lorem
                Ipsum, you need to be sure there isn't anything embarrassing
                hidden in the middle of text. All the Lorem Ipsum generators on
                the Internet tend to repeat predefined chunks as necessary,
                making this the first true generator on the Internet. It uses a
                dictionary of over 200 Latin words, combined with a handful of
                model sentence structures, to generate Lorem Ipsum which looks
                reasonable. The generated Lorem Ipsum is therefore always free
                from repetition, injected humour, or non-characteristic words
                etc.
              </Paragraph>
            </TitleBox>
          </SectionBox>
        </Section>
        <Section className="none-image-section">
          <SectionBox>
            <TitleBox className="none-image-box">
              <Title className="none-image-title">Lorem Ipsum</Title>
              <Paragraph className="none-image-paragraph">
                Contrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45 BC, making it over 2000 years old. Richard McClintock, a
                Latin professor at Hampden-Sydney College in Virginia, looked up
                one of the more obscure Latin words, consectetur, from a Lorem
                Ipsum passage, and going through the cites of the word in
                classical literature, discovered the undoubtable source. Lorem
                Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus
                Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
                written in 45 BC. This book is a treatise on the theory of
                ethics, very popular during the Renaissance. The first line of
                Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line
                in section 1.10.32.
              </Paragraph>
            </TitleBox>
          </SectionBox>
        </Section>
      </AboutMain>
      <Footer />
    </>
  );
};
