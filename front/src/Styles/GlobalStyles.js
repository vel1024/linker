import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";

export default createGlobalStyle`
    ${reset}
    html {
        box-sizing: border-box;
        font-size: 16px;
    }
    body {
        max-width: 100%;
        min-height: 100vh;
        overflow-x: hidden;
        display:flex;
        flex-direction:column;
    }
    li {
        list-style: none;
    }
    a {
        color: black;
        text-decoration:none;
    }
    input:focus {
        outline:none;
    }
    #root {
        max-width: 100%;
        min-height: 100vh;
        overflow: hidden;
        display:flex;
        flex-direction:column;    
    }
`;
