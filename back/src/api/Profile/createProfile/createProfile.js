import { prisma } from "../../../utils";

export default {
  Mutation: {
    createProfile: async (_, args, context) => {
      const { bio, userId } = args;

      let newProfile;
      try {
        newProfile = await prisma.profile.create({
          data: {
            bio,
            user: {
              connect: {
                id: userId,
              },
            },
          },
        });
      } catch (error) {
        console.log(error);
      }
      return newProfile;
    },
  },
};
