import { prisma } from "../../../utils";
export default {
  Mutation: {
    deleteProfile: (_, args) => {
      const { profileId } = args;
      return prisma.profile.delete({
        where: {
          id: profileId,
        },
      });
    },
  },
};
