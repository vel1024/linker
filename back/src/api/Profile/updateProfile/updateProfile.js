import { prisma } from "../../../utils";

export default {
  Mutation: {
    updateProfile: async (_, args) => {
      const { profileId, bio } = args;
      let updatedProfile;
      try {
        updatedProfile = await prisma.profile.update({
          where: {
            id: profileId,
          },
          data: {
            bio,
          },
        });
      } catch (error) {
        console.log(error);
      }
      return updatedProfile;
    },
  },
};
