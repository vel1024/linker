import { prisma } from "../../utils";

export default {
  Profile: {
    user: (parent) => {
      prisma.profile.findOne({ where: { id: parent.id } }).user();
    },
  },
};
