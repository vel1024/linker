import { NEW_MESSAGE } from "../../../topics";

export default {
  Subscription: {
    subMessage: {
      subscribe: (_, __, { pubsub }) => {
        return pubsub.asyncIterator(NEW_MESSAGE);
      },
    },
  },
};
