import { prisma } from "../../../utils";
import { NEW_MESSAGE } from "../../../topics";

export default {
  Mutation: {
    createMessage: async (_, args, { pubsub }) => {
      const { text, creatorId, groupId } = args;
      const messageObj = await prisma.message.create({
        data: {
          text,
          creator: {
            connect: {
              id: creatorId,
            },
          },
          group: {
            connect: {
              id: groupId,
            },
          },
        },
        include: {
          creator: true,
          group: true,
        },
      });
      if (!messageObj) throw new Error("can't create message");
      pubsub.publish(NEW_MESSAGE, { subMessage: messageObj });
      return messageObj;
    },
  },
};
