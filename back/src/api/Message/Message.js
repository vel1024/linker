import { prisma } from "../../utils";

export default {
  Message: {
    creator: (parent) => {
      prisma.message.findOne({ where: { id: parent.id } }).creator();
    },
  },
};
