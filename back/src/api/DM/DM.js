import { prisma } from "../../utils";

export default {
  DirectMessage: {
    sender: (parent) => {
      prisma.directMessage.findOne({ where: { id: parent.id } }).sender();
    },
  },
};
