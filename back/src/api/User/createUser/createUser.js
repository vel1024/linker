import { prisma } from "../../../utils";

export default {
  Mutation: {
    createUser: (_, args) => {
      const { username, email, password } = args;
      return prisma.user.create({
        data: {
          username,
          email,
          password
        },
      });
    },
  },
};
