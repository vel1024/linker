import { prisma } from "../../../utils";

export default {
  Query: {
    searchUser: (_, args) => {
      return prisma.user.findOne({
        where: {
          username: args.name,
        },
      });
    },
  },
};
