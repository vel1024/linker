import { prisma } from "../../utils";

export default {
  User: {
    profile: (parent) => {
      prisma.user.findOne({ where: { id: parent.id } }).profile();
    },
    friends: (parent) => {
      prisma.user.findOne({ where: { id: parent.id } }).friends();
    },
    directMessages: (parent) => {
      prisma.user.findOne({ where: { id: parent.id } }).directMessages();
    },
    messages: (parent) => {
      prisma.user.findOne({ where: { id: parent.id } }).messages();
    },
    participatedGroup: (parent) => {
      prisma.user.findOne({ where: { id: parent.id } }).participatedGroup();
    },
  },
};
