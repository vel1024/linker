import { prisma } from "../../utils";

export default {
  Friend: {
    friend: (parent) => {
      prisma.friend.findOne({ where: { id: parent.id } }).friend();
    },
  },
};
