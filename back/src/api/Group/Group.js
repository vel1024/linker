import { prisma } from "../../utils";

export default {
  Group: {
    users: (parent) => {
      prisma.group.findOne({ where: { id: parent.id } }).users();
    },
  },
};
