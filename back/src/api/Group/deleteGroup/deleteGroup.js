import { prisma } from "../../../utils";

export default {
  Mutation: {
    deleteGroup: (_, args) => {
      const { name } = args;
      return prisma.group.delete({
        where: {
          name,
        },
      });
    },
  },
};
