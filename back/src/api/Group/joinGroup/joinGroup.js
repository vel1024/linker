import { prisma } from "../../../utils";

export default {
  Mutation: {
    joinGroup: (_, args) => {
      const { userId, groupId } = args;
      return prisma.group.update({
        where: {
          id: groupId,
        },
        data: {
          users: {
            connect: {
              id: userId,
            },
          },
        },
      });
    },
  },
};
