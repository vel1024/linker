import { prisma } from "../../../utils";

export default {
  Mutation: {
    createGroup: (_, args) => {
      const { name } = args;
      return prisma.group.create({
        data: {
          name,
        },
      });
    },
  },
};
