import dotenv from "dotenv";
dotenv.config();
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import csp from "helmet-csp";
import expressPlayground from "graphql-playground-middleware-express";
import { ApolloServer, PubSub } from "apollo-server-express";
import morgan from "morgan";
import "./passport";
import { authenticateJWT } from "./passport";
import schema from "./schema";

const PORT = process.env.PORT;

const pubsub = new PubSub();

const server = new ApolloServer({
  schema,
  context: ({ req, res }) => ({ req, res, pubsub }),
});

const app = express();
app.use(helmet());

app.use(
  csp({
    directives: {
      defaultSrc: ["'self'"],
      styleSrc: ["'self'", "'unsafe-inline'"],
      styleSrcElem: [
        "'self'",
        "fonts.googleapis.com",
        "cdn.jsdelivr.net",
        "'unsafe-inline'",
      ],
      imgSrc: ["'self'", "cdn.jsdelivr.net"],
      scriptSrcElem: ["'self'", "cdn.jsdelivr.net", "'unsafe-inline'"],
      fontSrc: ["'self'", "'unsafe-inline'", "fonts.gstatic.com"],
    },
  })
);

app.use(bodyParser.json(), cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan("dev"));

app.use(authenticateJWT);

app.get("/", expressPlayground({ endpoint: "/graphql" }));

server.applyMiddleware({ app });

const handleListening = () => {
  console.log(`Server ready at http://localhost:${PORT}`);
};

app.listen(PORT, handleListening);
